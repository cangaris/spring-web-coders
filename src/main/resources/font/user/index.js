function renderTbody(user, selector) {
	$(selector + " tbody").append(
		`<tr>
			<td>${user.id}</td>
			<td>${user.firstName}</td>
			<td>${user.lastName}</td>
			<td>${user.age}</td>
			<td>${user.city}</td>
		</tr>`
	)
}

function getData(selector, httpObject) {
	$(selector + " tbody").empty();
	$(selector + " .errors").addClass("d-none");
	$.ajax(httpObject)
		.done(
			(data) => { // kiedy przyjdzie kod 200 {"5":"Asia", "7":"Ania"}
				const isArray = _.isArray(data); // loadash -> _ (podłoga)
				if (isArray) {
					for(let index in data) {
						renderTbody(data[index], selector);
					}
				} else {
					renderTbody(data, selector);
				}
			}
		)
		.fail(
			(error) => { // kiedy przyjdzie kod zaczynający się na 400 lub 500
				const errorLabel = error.responseJSON.message;
				$(selector + " .errors").removeClass("d-none").append(errorLabel);
			}
		);
}
function getUsers () {
	getData("#users", {url: `//localhost:8090/user`, type: 'GET'});
}
function getUserById () {
	const userId = $("#user .userId").val();
	getData("#user", {url: `//localhost:8090/user/${userId}`, type: 'GET'});
}
function addNewUser () {
	const firstName = $("#add .firstName").val();
	const lastName = $("#add .lastName").val();
	const age = $("#add .age").val();
	const city = $("#add .city").val();
	getData("#add", {
		url: `//localhost:8090/user`,
		type: 'POST',
		data: JSON.stringify({firstName, lastName, age, city}),
		contentType: "application/json",
		dataType: 'json',
	});
}
function updateUser () {
	const userId = $("#update .userId").val();
	const firstName = $("#update .firstName").val();
	const lastName = $("#update .lastName").val();
	const age = $("#update .age").val();
	const city = $("#update .city").val();
	/**
	 * {firstName: firstName} jeżeli w obiekcie klucz i wartość są tak samo nazwane to można użyć
	 * składni {firstName} - jest to tak zwany "syntax sugar"
	 */
	getData("#update", {
		url: `//localhost:8090/user/${userId}`,
		type: 'PUT',
		data: JSON.stringify({id: userId, firstName: firstName, lastName, age, city}),
		contentType: "application/json",
		dataType: 'json',
	});
}
function deleteUser () {
	const userId = $("#remove .userId").val();
	const selector = "#remove";
	const object = {url: `//localhost:8090/user/${userId}`, type: 'DELETE'};
	getData(selector, object);
}