function renderTbody(product, selector) {
	$(selector + " tbody").append(
		`<tr>
			<td>${product.id}</td>
			<td>${product.name}</td>
			<td>${product.price}</td>
			<td>${product.weight}</td>
		</tr>`
	)
}

function getData(selector, httpObject) {
	$(selector + " tbody").empty();
	$(selector + " .errors").addClass("d-none");
	$.ajax(httpObject)
		.done(
			(data) => { // kiedy przyjdzie kod 200 {"5":"Asia", "7":"Ania"}
				const isArray = _.isArray(data); // loadash -> _ (podłoga)
				if (isArray) {
					for(let index in data) {
						renderTbody(data[index], selector);
					}
				} else {
					renderTbody(data, selector);
				}
			}
		)
		.fail(
			(error) => { // kiedy przyjdzie kod zaczynający się na 400 lub 500
				const errorLabel = error.responseJSON.message;
				$(selector + " .errors").removeClass("d-none").append(errorLabel);
			}
		);
}
function getProducts () {
	getData("#products", {url: `//localhost:8090/product`, type: 'GET'});
}
function getProductById () {
	const productId = $("#product .productId").val();
	getData("#product", {url: `//localhost:8090/product/${productId}`, type: 'GET'});
}
function addNewProduct () {
	const name = $("#add .name").val();
	const price = $("#add .price").val();
	const weight = $("#add .weight").val();
	getData("#add", {
		url: `//localhost:8090/product`,
		type: 'POST',
		data: JSON.stringify({name, price, weight}),
		contentType: "application/json",
		dataType: 'json',
	});
}
function updateProduct () {
	const productId = $("#update .productId").val();
	const name = $("#update .name").val();
	const price = $("#update .price").val();
	const weight = $("#update .weight").val();
	/**
	 * {name: name} jeżeli w obiekcie klucz i wartość są tak samo nazwane to można użyć
	 * składni {name} - jest to tak zwany "syntax sugar"
	 */
	getData("#update", {
		url: `//localhost:8090/product/${productId}`,
		type: 'PUT',
		data: JSON.stringify({id: productId, name: name, price, weight}),
		contentType: "application/json",
		dataType: 'json',
	});
}
function deleteProduct () {
	const productId = $("#remove .productId").val();
	const selector = "#remove";
	const object = {url: `//localhost:8090/product/${productId}`, type: 'DELETE'};
	getData(selector, object);
}