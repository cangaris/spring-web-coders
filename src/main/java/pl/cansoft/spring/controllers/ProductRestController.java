package pl.cansoft.spring.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import pl.cansoft.spring.exceptions.NotFoundException;
import pl.cansoft.spring.models.Product;
import pl.cansoft.spring.repository.ProductRepository;
import pl.cansoft.spring.services.ProductService;

import java.util.List;

@RequiredArgsConstructor
@CrossOrigin("*")
@RestController
@RequestMapping("/product")
public class ProductRestController {

	final ProductService productService;

	@GetMapping
	List<Product> getProducts() {
		return productService.getAllProducts();
	}

	@GetMapping("/{id}")
	Product getProductById(@PathVariable Integer id) {
		var optionalProduct = productService.getProductById(id);
		if (optionalProduct.isPresent()) {
			return optionalProduct.get(); // domyślnie zwaraca kod 200 (OK)
		} else {
			throw new NotFoundException(); // kod 404 zwracamy jako nasz własny wyjątek
		}
	}

	@PostMapping
	Product addProduct(@RequestBody Product product) {

//		var productNew = new Product(); // tworzenie obiektu klasycznie za pomocą konstruktora
//		productNew.setDate(null);
//		productNew.setImage(null);
//		productNew.setPrice(null);
//		productNew.setWeight(null);
//		productNew.setName(null);
//		productNew.setId(1);

//		var productNew = Product.builder() // tworzenie obiektu za pomocą buildera
//				.id(1)
//				.date(null)
//				.image(null)
//				.name(null)
//				.price(null)
//				.weight(null)
//				.build();

		return productService.saveProduct(product);
	}

	@PutMapping("/{id}")
	Product updateProduct(@PathVariable Integer id, @RequestBody Product product) {
		var optionalProduct = productService.getProductById(id);
		if (optionalProduct.isPresent()) {
			return productService.saveProduct(product);
		} else {
			throw new NotFoundException();
		}
	}

	@DeleteMapping("/{id}")
	Product delProductById(@PathVariable Integer id) {
		var optionalProduct = productService.getProductById(id);
		if (optionalProduct.isPresent()) {
			var storedProduct = optionalProduct.get();
			productService.deleteIfExists(id);
			return storedProduct;
		} else {
			throw new NotFoundException();
		}
	}
}
