package pl.cansoft.spring.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import pl.cansoft.spring.exceptions.NotFoundException;
import pl.cansoft.spring.exceptions.NotValidException;
import pl.cansoft.spring.models.User;
import pl.cansoft.spring.repository.UserRepository;
import pl.cansoft.spring.services.UserService;

import javax.validation.Valid;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * CRUD:
 * - C - Create
 * - R - Read
 * - U - Update
 * - D - Delete
 */
@RequiredArgsConstructor // tworzy konstruktor pól wymaganych (oznaczonych słowem final)
@CrossOrigin("*") // wyłączenie domyślego sposobu ochrony przed atakami z innych domen
@RestController
@RequestMapping("user") // nadanie prefixu dla all endpointów używanych w klasie
public class UserRestController {

	/**
	 * Map.of i Map.ofEntries tworzy stałą niezmienialną wersję MAPy - nie mogę jej później rozszerzać
	 * Jeśli chciałbym Mapę dynamiczną powinieniem utworzyć ją poprzez konstruktor -> new HashMap<>()
	 * const json = {
	 * 	     id: 1, // key - klucz obiektu (id, name)
	 * 	     name: 'Damian' // value - wartość obiektu (1, 'Damian')
	 * 	 }
	 * private Map<Integer, User> users = new HashMap<>();
	 */

	final UserService userService;

	/**
	 * READ (ALL)
	 */
	@GetMapping // Endpoint pokaż wszystkich usersów
	List<User> getUsers() {
		return userService.getAllUsers();
	}

	/**
	 * READ (by ID)
	 */
	@GetMapping("/{id}") // Pokaż tylko konkretnego po zadanym ID
	User getUserById(@PathVariable Integer id) {
		var optionalUser = userService.getUserById(id);
		if (optionalUser.isPresent()) {
			return optionalUser.get();
		} else {
			throw new NotFoundException(); // return "Użytkownik o ID: " + id + " nie został odnaleziony!";
		}
	}

	/**
	 * CREATE - POST przy dodawaniu nowego rekordu
	 * const arr = [10,20,30,40]
	 * arr.length // 4
	 * arr[arr.length - 1] -> 40
	 */
	@PostMapping // Endpoint pozwalający na dodanie elementu do MAPy users
	User addUser(@RequestBody User user) {
		return userService.saveUser(user);
	}

	/**
	 * UPDATE - PUT, aktualizacja istniejącego rekordu
	 */
	@PutMapping("/{id}") // Endpoint pozwalający na update elementu w MAPie po konkretnym ID
	User updateUser(@PathVariable Integer id, @Valid @RequestBody User user, BindingResult results) {
		if (results.hasErrors()) {
			var errorMessage = results.getAllErrors().get(0).getDefaultMessage();
			throw new NotValidException(errorMessage);
		}
		//		age: "50"
		//		city: "Warszawa"
		//		firstName: "Jan"
		//		lastName: "Kowalski"
		var optionalUser = userService.getUserById(id);
		if (optionalUser.isPresent()) {
			// user.setId(id);
			//      id: 2
			//		age: "50"
			//		city: "Warszawa"
			//		firstName: "Jan"
			//		lastName: "Kowalski"
			return userService.saveUser(user);
		} else {
			throw new NotFoundException();
		}
	}

	/**
	 * DELETE
	 */
	@DeleteMapping("/{id}")
	User delUserById(@PathVariable Integer id) {
		var optionalUser = userService.getUserById(id);
		if (optionalUser.isPresent()) {
			var storedUser = optionalUser.get();
			userService.deleteIfExists(id);
			return storedUser;
		} else {
			throw new NotFoundException();
		}
	}
}

//	Map<Integer, String> users = Map.ofEntries(
//			Map.entry(1, "Damian"),
//			Map.entry(2, "Zosia"),
//			Map.entry(3, "Basia")
//	);
