package pl.cansoft.spring.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import pl.cansoft.spring.models.Product;
import pl.cansoft.spring.models.User;
import pl.cansoft.spring.services.ProductService;
import pl.cansoft.spring.services.UserService;

import javax.validation.Valid;

/**
 * onet.pl:443 -> [adres IP] 189.124.233.10:443 -> [serwery DNS]
 * MySQL 3306,
 * Spring -> 8080,
 * Angular -> 4200,
 * Node.js -> 3000,
 * Porty to są sloty pod którymi wystawione są usługi i czekają na interakcję z klientem,
 * // @Controller [Komunikacja HTML - aplikacje statyczne],
 * // @RestController // [Komunikacja JSON - aplikacje dynamiczne]
 * // @GetMapping // Wystawienie endpointa na świat pod daną ścieżką
 * xampp jest serwerem interpretacyjnym dla PHP, JAVA jest serwowana przez Tomcat
 */
@Controller
@RequiredArgsConstructor
@RequestMapping("/mvc/user")
public class UserController {

	/**
	 * HTTP <-> Controller (otrzymymuje dane i zwraca) <-> Service (logika biznesowa) <-> Repository (wykonują zapytania do bazy) <-> DB
	 */
	final UserService userService;

	/**
	 * Dependency Injection - wstrzykiwanie zależności (IoC - inversion of control)
	 * Uruchamia się Spring - skanuje wszytskie pliki i szuka adnotacji
	 * Jeśli znajdzie adnotacje nad klasą to rejestruje je w pamięci i w każdym momencie potem - można ich użyć
	 * MvcController(UserRepository userRepository) - dostarcz mi repozytorium użytkownika i zapisz do zmiennej
	 */
//	public UserController(UserService userService) {
//		this.userService = userService;
//	}

	/**
	 * Gdy korzystaliśmy z MAPy struktura była:
	 * [
	 *  1 (getKey()) : User{firstName='Damian', lastName='Cangaris', age=31, city='Wrocław'} (getValue()),
	 *  2 : User{firstName='Damian', lastName='Cangaris', age=31, city='Wrocław'},
	 * ]
	 * Po zmianie na bazę danych, nie ma już Mapy tylko LISTa:
	 * [
	 *  User{id: 1, firstName='Damian', lastName='Cangaris', age=31, city='Wrocław'},
	 *  User{id: 2, firstName='Damian', lastName='Cangaris', age=31, city='Wrocław'},
	 * ]
	 */
	@GetMapping
	String getAllUsers(Model model) {
		var users = userService.getAllUsers();
		model.addAttribute("users", users);
		return "mvc-all-users"; // src/main/resources/templates/mvc-all-users.html
	}

	@GetMapping("/{id}")
	String getSingleUserById(Model model, @PathVariable Integer id) {
		var optionalUser = userService.getUserById(id);
		if (optionalUser.isPresent()) {
			model.addAttribute("user", optionalUser.get());
			return "mvc-single-user"; // src/main/resources/templates/mvc-single-user.html
		} else {
			model.addAttribute("notFoundLabel", "Użytkownik nie odnaleziony");
			return "404"; // src/main/resources/templates/404.html
		}
	}

	@GetMapping("/{id}/edit")
	String showUpdateUserForm(Model model, @PathVariable Integer id) {
		var optionalUser = userService.getUserById(id);
		if (optionalUser.isPresent()) {
			model.addAttribute("user", optionalUser.get());
			return "mvc-update-user";
		} else {
			model.addAttribute("notFoundLabel", "Użytkownik nie odnaleziony");
			return "404";
		}
	}

	/**
	 * metoda odpowiadająca za pokazanie formularza oraz
	 * dorzucenie do niego ustego obiektu User czekającego na wypełenienie przez formularz
	 * za wyświetlanie formularzy powinny odpowiadać metody GET, zaś metody POST PUT DEL modyfukujące dane
	 * zawsze powinny po akcji wykonać redirect w podejściu MVC na jakiegoś GETa!
	 */
	@GetMapping("/add")
	String showAddUserForm(Model model, User user) { // "results" istnieje po redirect!
	    model.addAttribute("results", (BindingResult) model.getAttribute("results")); // ta linijka jest tylko po to aby w HTMLu znane było typowanie results jako BindingResults
		model.addAttribute("user", user);
		return "mvc-add-user";
	}

	/**
	 * po wypełnieniu formularza i kliknięciu zapisz
	 * uruchamia się action formularza, obiekt user jest setowany za pomocą setterów z klasy User
	 * i przychodzi jako parametr "User user" z wypełnionymi danymi gotowymi do zapisu
	 * poprzez obiekt userRepository !!!
	 */
	@PostMapping("/save")
	String saveUserToDatabase(@Valid User user, BindingResult results, RedirectAttributes attr) {
		if (results.hasErrors()) {
			attr.addFlashAttribute("results", results); // przekazanie zmiennych przy redirect
		    attr.addFlashAttribute("user", user); // przekazanie danych z wcześniej wypełnionego folumarza jeszcze raz do korekty wraz z listą błędów
			return "redirect:/mvc/user/add";
		} else {
			userService.saveUser(user);
			return "redirect:/mvc/user";
		}
	}

	@PostMapping("/{id}/update")
	String updateUserToDatabase(User user, @PathVariable Integer id) {
		userService.saveUser(user);
		return "redirect:/mvc/user/" + id;
	}

	@GetMapping("/{id}/delete")
	String deleteUser(@PathVariable Integer id) {
		userService.deleteIfExists(id);
		return "redirect:/mvc/user";
	}
}
