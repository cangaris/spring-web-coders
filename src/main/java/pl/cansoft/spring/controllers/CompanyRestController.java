package pl.cansoft.spring.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import pl.cansoft.spring.models.Company;
import pl.cansoft.spring.models.Product;
import pl.cansoft.spring.repository.CompanyRepository;
import pl.cansoft.spring.services.ProductService;

import java.util.List;

@RequiredArgsConstructor
@CrossOrigin("*")
@RestController
@RequestMapping("/company")
public class CompanyRestController {

	/**
	 * użycie repozytorium w controllerze jest niezalecane
	 * użyte tylko do celów ćwiczeniowych - tak na prawdę powinien być
	 * CompanyService a w nim CompanyRepository !!!
	 */
	final CompanyRepository companyRepository;

	@Secured({"ROLE_ADMIN", "ROLE_USER"})
	@GetMapping
	List<Company> getCompanies() {
		return companyRepository.findAll();
	}

	@Secured("ROLE_ADMIN")
	@PostMapping
	Company getCompanies(@RequestBody Company company) {
		return companyRepository.save(company);
	}
}
