package pl.cansoft.spring.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import pl.cansoft.spring.models.Product;
import pl.cansoft.spring.models.User;
import pl.cansoft.spring.repository.ProductRepository;
import pl.cansoft.spring.repository.UserRepository;
import pl.cansoft.spring.services.ProductService;
import pl.cansoft.spring.services.UserService;

import javax.validation.Valid;

/**
 * MVC - model view controller - aplikacja, która ma swoje widoki na backendzie
 * sposób synchroniczny
 * brak PUT/DELETE - MVC obsługuje tylko GET i POST (PUT robi się jako POST, DELETE jako GET)
 * narzędzie do serwowania kody HTML dla stron statycznych dla Javy/Spring - Thymeleaf
 * instalacja Thymeleaf => https://www.baeldung.com/spring-boot-crud-thymeleaf
 * baza danych H2 => https://www.baeldung.com/spring-boot-h2-database
 * encje => https://www.baeldung.com/jpa-entities
 * dokumentacja thymleaf => https://www.thymeleaf.org/doc/tutorials/2.1/usingthymeleaf.html
 * dostęp do konsoli bazy danych H2 => http://localhost:8090/h2-console
 *
 * // async - rest
 * FRONT: http://localhost:63342 -> frontend.pl
 * BACK: http://localhost:8090 -> backend.pl
 * DB: http://localhost:3306 -> database.pl
 * frontend.pl | fontend2.pl | front-xxx.pl <-> backend.pl <-> database.pl
 *
 * // sync - html (MVC)
 * FRONT = BACK -> ten sam port
 * FRONT: http://localhost:8090 -> backend.pl
 * BACK: http://localhost:8090 -> backend.pl
 * backend.pl <-> database.pl
 */
@RequiredArgsConstructor
@Controller
@RequestMapping("/mvc/product")
public class ProductController {

	/**
	 * HTTP <-> Controller (otrzymymuje dane i zwraca) <-> Service (logika biznesowa) <-> Repository (wykonują zapytania do bazy) <-> DB
	 */
	final ProductService productService;

	/**
	 * Dependency Injection - wstrzykiwanie zależności (IoC - inversion of control)
	 * Uruchamia się Spring - skanuje wszytskie pliki i szuka adnotacji
	 * Jeśli znajdzie adnotacje nad klasą to rejestruje je w pamięci i w każdym momencie potem - można ich użyć
	 * MvcController(UserRepository userRepository) - dostarcz mi repozytorium użytkownika i zapisz do zmiennej
	 */
//	public ProductController(ProductService productService) {
//		this.productService = productService;
//	}

	@GetMapping("/add")
	String showAddProductForm(Model model, Product product) {
		model.addAttribute(product);
		return "mvc-add-product";
	}

	@PostMapping("/save")
	String saveProductToDatabase(@Valid Product product, BindingResult results, RedirectAttributes attr) {
		if (results.hasErrors()) {
			attr.addFlashAttribute("hasError", results.hasErrors());
			return "redirect:/mvc/product/add";
		} else {
			productService.saveProduct(product);
			return "redirect:/mvc/product";
		}
	}

	@GetMapping("/{id}/delete")
	String deleteProduct(@PathVariable Integer id) {
		productService.deleteIfExists(id);
		return "redirect:/mvc/product";
	}

	@GetMapping
	String getAllProducts(Model model) {
		var products = productService.getAllProducts();
		model.addAttribute("products", products);
		return "mvc-all-products"; // src/main/resources/templates/mvc-all-users.html
	}

	@GetMapping("/{id}")
	String getSingleProductById(Model model, @PathVariable Integer id) {
		var optionalProduct = productService.getProductById(id);
		if (optionalProduct.isPresent()) {
			model.addAttribute("product", optionalProduct.get());
			return "mvc-single-product";
		} else {
			model.addAttribute("notFoundLabel", "Produkt nie został odnaleziony!");
			return "404"; // src/main/resources/templates/404.html
		}
	}
}
