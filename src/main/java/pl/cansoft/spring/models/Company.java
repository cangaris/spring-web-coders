package pl.cansoft.spring.models;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
public class Company {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(length = 127)
    private String name;

    @OneToOne(cascade = CascadeType.ALL)
    private Vat vat;

    /**
     * CascadeType.ALL pozwala na kaskadowe dodawanie, usuwanie obiektów
     * tzn można dodać firmę razem w obiektem VAT oraz listą employee
     * jest to wyrażenie zgody na zapisywanie obiektów z podobiektami
     */
    @OneToMany(cascade = CascadeType.ALL)
    private List<Employee> employee;

    @ManyToMany
    private List<Address> address;
}
