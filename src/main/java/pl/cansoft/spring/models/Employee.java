package pl.cansoft.spring.models;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.Size;

@Data
@Entity
public class Employee {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    // @Column(columnDefinition = "varchar(50)")
    @Column(length = 50) // definicja rozmiaru w bazie
    @Size(min = 3, max = 50) // walidacja kiedy front wysyła dane
    private String firstName;

    private String lastName;
}
