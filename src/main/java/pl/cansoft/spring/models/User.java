package pl.cansoft.spring.models;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.*;

/**
 * GenerationType.IDENTITY vs
 * User 1 2
 * Product 1 2
 *
 * GenerationType.AUTO | brak
 * User 2 3 6 8 9
 * Product 1 2 4 5 7
 * Category 10
 */
@NoArgsConstructor
@Data
@Entity // wymagane oznaczenie klasy User jako tabeli w bazie danych
public class User {

	@Id // wymagane jako klucz podstawowy jeśli klasa User ma być tabelą w bazie damych
	@GeneratedValue(strategy = GenerationType.IDENTITY) // wyjaśnienie u góry
	private Integer id;
	@NotBlank(message = "Pole imię nie może być puste")
	@Size(min = 3, max = 30, message = "Pole imię musi mieć od 3 do 30 znaków")
	private String firstName;
	@NotBlank(message = "Pole imię nie może być puste")
	@Size(min = 3, max = 30, message = "Pole imię musi mieć od 3 do 30 znaków")
	private String lastName;
	@NotNull(message = "Pole wiek jest wymagane")
	@Min(value = 18, message = "Wiek minimalny to 18 lat")
	@Max(value = 100, message = "Wiek maksymalny to 100 lat")
	private Integer age;
	@NotBlank(message = "Pole imię nie może być puste")
	@Size(min = 3, max = 30, message = "Pole imię musi mieć od 3 do 30 znaków")
	private String city;
	@NotBlank(message = "Pole email nie może być puste")
	@Size(min = 3, max = 30, message = "Pole email musi mieć od 3 do 30 znaków")
	@Email
	private String email;
	@NotBlank(message = "Pole telefon nie może być puste")
	@Size(min = 3, max = 30, message = "Pole telefon musi mieć od 3 do 30 znaków")
	private String phone;
	@NotBlank(message = "Pole obrazek nie może być puste")
	@Size(min = 3, max = 255, message = "Pole obrazek musi mieć od 3 do 30 znaków")
	private String image;
	@NotBlank(message = "Pole tytuł nie może być puste")
	@Size(min = 3, max = 30, message = "Pole tytuł musi mieć od 3 do 30 znaków")
	private String title;
	private String password;
	private String role;
}
