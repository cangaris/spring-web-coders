package pl.cansoft.spring.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.*;
import java.time.LocalDateTime;

// @AllArgsConstructor
@NoArgsConstructor // tworzy pusty konstruktor za pomocą lombok
@Data // tworzy gettery i settery za pomocą lombok
// @Builder // tworzy builder (wzorzec projektowy do setowania obiektów)
@Entity
public class Product {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	@NotBlank
	@Size(min = 3, max = 20)
	private String name;
	@NotNull
	@Min(20)
	@Max(10000)
	private Integer price;
	@NotNull
	@Min(1)
	@Max(200)
	private Integer weight;
	@NotBlank
	@Size(min = 3, max = 255)
	private String image;
	@FutureOrPresent
	private LocalDateTime date;
}
