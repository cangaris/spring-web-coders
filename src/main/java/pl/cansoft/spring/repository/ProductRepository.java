package pl.cansoft.spring.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.cansoft.spring.models.Product;

@Repository
public interface ProductRepository extends JpaRepository<Product, Integer> {
}
