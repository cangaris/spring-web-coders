package pl.cansoft.spring.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.cansoft.spring.models.User;

import java.util.Optional;

/**
 * repozytoria to specjalne bloki w Springu, dzięki którym pobieramy dane z bazy
 * dodajemy, aktualizujemy, usuwany etc
 * JpaRepository odpowiedzialne jest za metody dodawania, usuwania, pobierania etc - nie musimy pisać własnych!
 * <User, Integer> - jest wymagane określenie jakie dane pod jakim kluczem są w bazie
 */
@Repository
public interface UserRepository extends JpaRepository<User, Integer> {

    Optional<User> findByEmail(String email);
}
