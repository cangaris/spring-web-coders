package pl.cansoft.spring.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(
		code = HttpStatus.NOT_FOUND,
		reason = "Żądany zasób nie został odnaleziony"
)
public class NotFoundException extends RuntimeException {
}
