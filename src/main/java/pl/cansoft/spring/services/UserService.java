package pl.cansoft.spring.services;

import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import pl.cansoft.spring.models.User;
import pl.cansoft.spring.repository.UserRepository;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Service
public class UserService {

	final UserRepository userRepository;

	public List<User> getAllUsers() {
		return userRepository.findAll();
	}

	public Optional<User> getUserById (Integer id) {
		return userRepository.findById(id);
	}

	public Optional<User> getUserByEmail (String email) {
		return userRepository.findByEmail(email);
	}

	public User saveUser(User user) {
		var encoder = new BCryptPasswordEncoder();
		var encoded = encoder.encode(user.getPassword());
		user.setPassword(encoded);
		user.setRole("USER");
		return userRepository.save(user);
	}

	public void deleteIfExists(Integer id) {
		if (userRepository.existsById(id)) {
			userRepository.deleteById(id);
		}
	}
}
